# TeaCode React.js Recruitment Challenge
By Anna Piasecka.
This project was bootstrapped with Gatsby.js

## How to run the project locally

Navigate into site’s directory and do following:

```
npm install
npm run develop
```
