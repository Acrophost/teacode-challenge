import styled from 'styled-components';

export const StyledBar = styled.section`
    width: 100%;
    position: fixed;
    top: 5rem;
    z-index: 100;
    display: flex;
    flex-direction: row;
    align-items: center;
    border-bottom: 1px solid #999;
    background: white;
`;

export const StyledInput = styled.input`
    width: 100%;
    padding: 1rem 0;
    border: none;
    outline: none;
    cursor: text;
    padding-left: 1rem;
`;

export const StyledIcon = styled.img`
    width: 1.5rem;
    margin-left: 1rem;
`;