import styled from 'styled-components';

export const Container = styled.div`
    width: 100%;
    min-height: 100vh;
    margin: 0;
    padding: 0;
    background: linear-gradient(to left, #f4f4f4, #DFE0E2);
`;

export const StyledHeader = styled.header`
    width: 100%;
    margin: 0;
    padding: 0 2rem;
    text-align: center;
    position: fixed;
    top: 0;
    background: linear-gradient(to left, #6B2737, #ab3450);
    color: white;
    letter-spacing: .2rem;
    z-index: 100;
`;

export const StyledMain = styled.main`
    top: 0;
    margin-top: 8rem;
`;