import styled from 'styled-components';

export const StyledContact = styled.button`
    cursor: pointer;
    border: none;
    border-bottom: 1px solid #AAAAAAAA;
    width: 100%;
    display: flex;
    flex-direction: row;
    align-items: center;
    outline: none;
    position: relative;
`;

export const StyledAvatar = styled.img`
    border-radius: 50%;
    width: 2.8rem;
    height: 2.8rem;
    margin-right: 1rem;
`;

export const ImprovisedAvatar = styled.div`
    border-radius: 50%;
    background: white;
    display: flex;
    justify-content: center;
    align-items: center;
    text-align: center;
    width: 2.8rem;
    height: 2.8rem;
    margin-right: 1rem;
    padding: 0;
    letter-spacing: -.2rem;
    font-size: 1.2rem;
    color: #999;
    box-shadow: inset 0 0 0 2px #999;
`;