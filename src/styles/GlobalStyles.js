import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
    *,
    *:before,
    *:after {
        box-sizing: border-box;
    }

    html,
    body {
        overflow-x: hidden;
        margin: 0;
        padding: 0;
    }
    `;

export default GlobalStyle;