import * as React from "react"

import Contacts from '../components/Contacts';
import SearchBar from '../components/SearchBar';

import GlobalStyle from './../styles/GlobalStyles';
import { Container, StyledHeader, StyledMain } from '../styles/Index_Wrapper';

const ContactsPage = () => {
  const [users, setUsers] = React.useState([]);
  const [searchedUsers, setSearchedUsers] = React.useState([]);
  const [loadingState, setLoadingState] = React.useState({state: 'loading', error: ''});
  const [input, setInput] = React.useState('');

  React.useEffect(() => {
    fetch(`https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json`)
      .then(response => response.json())
      .then(response => {
        response.sort((a,b) => a.last_name.toLowerCase().localeCompare(b.last_name.toLowerCase()));
        setUsers(response);
        setSearchedUsers(response);
        setLoadingState({state: 'loaded'});
      })
      .catch(err => {
        setLoadingState({error: err});
      });
  },[]);

  const updateInput = async (keyword) => {
    let result = users.filter(user => {
      return user.first_name.toLowerCase().includes(input.toLowerCase()) || user.last_name.toLowerCase().includes(input.toLowerCase());
    })

    result.sort((a,b) => a.last_name.toLowerCase().localeCompare(b.last_name.toLowerCase()));

    setInput(keyword);

    if(keyword === '') result = users;

    setSearchedUsers(result);
  }

  return (
    <Container>
    <GlobalStyle />
    {loadingState.state === 'loaded' ? 
      <StyledMain>
        <StyledHeader>
          <h1>Contacts</h1>
        </StyledHeader>
        <SearchBar input={input} setInput={updateInput} />
        <Contacts users={searchedUsers} />
      </StyledMain> :
      <>
        {loadingState.error !== '' 
          ? `${loadingState.error}` 
          : `Loading...`
        }
      </>
    }
    </Container>
  )
}

export default ContactsPage
