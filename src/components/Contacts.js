import * as React from 'react';

import SingleContact from './SingleContact';

const Contacts = ({users}) => {
    const [selectedIds, setSelectedIds] = React.useState([]);

    const ContactClicked = () => {
        console.log('Selected users: ' + selectedIds.join(' '));
    };

    return (
    <section>
        {users.map((val, n) => (
            <SingleContact user={val} key={n} clicked={ContactClicked} selectedIds={selectedIds} setSelectedIds={setSelectedIds} />
        ))}
    </section>
    );
}

export default Contacts;