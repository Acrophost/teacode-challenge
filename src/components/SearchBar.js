import * as React from 'react';

import { StyledBar, StyledInput, StyledIcon } from '../styles/SearchBar_Wrapper';

import SearchIcon from '../images/search.svg';

const SearchBar = ({ input, setInput }) => {
    return (
        <StyledBar>
            <StyledIcon src={SearchIcon} alt="Search" />
            <StyledInput value={input} onChange={(e) => {setInput(e.target.value)}} placeholder="All contacts" />
        </StyledBar>
    );
}

export default SearchBar;