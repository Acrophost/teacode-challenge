import * as React from 'react';
import { Checkbox } from '@material-ui/core';

import { StyledContact, StyledAvatar, ImprovisedAvatar } from '../styles/SingleContact_Wrapper';

const SingleContact = ({ user, clicked, selectedIds, setSelectedIds }) => {
    const [selected, setSelected] = React.useState(selectedIds.includes(user.id));

    const handleClick = () => {
        if(!selectedIds.includes(user.id)) {
            const newSelected = selectedIds;
            newSelected.push(user.id);
            setSelectedIds(newSelected);
        } else {
            const newSelected = selectedIds;
            if(newSelected[0] === user.id) newSelected.shift();
            else if(newSelected[newSelected.length - 1] === user.id) newSelected.pop();
            else {
                let index = 0;
                for(let i = 0; i < newSelected.length; i++) {
                    if(newSelected[i] === user.id) {
                        index = i;
                        break;
                    }
                }
                newSelected.splice(index, 1);
            }
            setSelectedIds(newSelected);
        }
        setSelected(selectedIds.includes(user.id));
        clicked();
    };

    const updateSelection = React.useCallback(() => {
        setSelected(selectedIds.includes(user.id));
    }, [user.id, selectedIds]);

    React.useEffect(updateSelection, [selectedIds, updateSelection]);

    return (
    <StyledContact onClick={handleClick}>
        {user.avatar 
            ? <StyledAvatar src={user.avatar} alt={`${user.first_name}'s avatar`} />
            : <ImprovisedAvatar>{`${user.first_name[0]} ${user.last_name[0]}`}</ImprovisedAvatar>
        }
        <h2>{user.first_name} {user.last_name}</h2>
        <Checkbox 
            checked={selected}
            value={selected}
            style={{color: '#558564'}}
        />
    </StyledContact>
    );
}

export default SingleContact;